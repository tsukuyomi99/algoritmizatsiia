#include <iostream>
#include <fstream>
#include "Header.h"
#include <cstdlib>
#include <iterator>
Polinom operator+(Polinom a,Polinom b)//перегрузка оператора сложения
{
    for(auto x=a.m.begin();x!=a.m.end();x++)
    {
        if(b.m.count((*x).first))
        {
            map<int,int>::iterator it=b.m.find((*x).first);
            if((*x).second+(*it).second)
            {
                a.m.at((*x).first)=(*x).second+(*it).second;
            }
            else
            {
                auto t=x;
                t--;
                a.m.erase(x);
                x=t;
            }
            b.m.erase(it);
        }
    }
    if(b.m.size()!=0)
    {
        a.m.insert(b.m.begin(),b.m.end());
    }
    Polinom z(a);
    return z;
}
Polinom operator-(Polinom a,Polinom b)//перегрузка оператора вычитания
{
    for(auto x=a.m.begin();x!=a.m.end();x++)
    {
        if(b.m.count((*x).first))
        {
            map<int,int>::iterator it=b.m.find((*x).first);
            if((*x).second-(*it).second)
            {
                a.m.at((*x).first)=(*x).second-(*it).second;
            }
            else
            {
                auto t=x;
                t--;
                a.m.erase(x);
                x=t;
            }
            b.m.erase(it);
        }
    }
    if(b.m.size()!=0)
    {
        a.m.insert(b.m.begin(),b.m.end());
    }
    Polinom z(a);
    return z;
}
Polinom operator*(Polinom a,Polinom b)//перегрузка оператора умножения
{
    Polinom z;
    for(auto x=a.m.begin();x!=a.m.end();x++)
    {
        for(auto q=b.m.begin();q!=b.m.end();q++)
        {
            int k=x->first+q->first;
            if(z.m.count(k))
            {
                if(z.m.find(k)->second+x->second*q->second)
                    z.m.find(k)->second+=x->second*q->second;
                else
                    z.m.erase(z.m.find(k));
            }
            else
                z.m.insert(pair<int,int>(x->first+q->first,x->second*q->second));
        }
    }
    return z;
}
ostream& operator << (ostream& os,Polinom q)//перегрузка оператора вывода
{
    if((q.m.size()==0))
    {
        cout<<"0"<<endl;
        return os;
    }
    for(auto x=q.m.begin();x!=q.m.end();x++)
    {
        if((*x).second==1)
            os<<"x^"<<(*x).first;
        else if((*x).second==-1)
            os<<"-x^"<<(*x).first;
        else
            os<<x->second<<"x^"<<(*x).first;
        x++;
        if(x==q.m.end())
            os<<endl;
        else if(x->second>0)
            os<<"+";
        x--;
    }
    return os;
}
