#include "Header.h"
void Unit::info()//метод базового класса
{
    cout << name << " " << level;
    cout << "lvl" << endl;
    cout << health << "remains" << endl;
    cout << "Attack=" << attack << " Health=" << health << endl;
}

void Unit :: heal()//метод базового класса
{
    health += level * 3;
}

void Monster :: creareM(int lvl)//метод обычного класса
{
    name = "Monster";
    level = lvl;
    attack = level * 2;
    defence = level;
    health = health * 10;
}

void Unit :: lvlup()//метод базового класса
{
    level++;
    health = health + (health/level);
    attack = attack + (attack/level);
    defence = defence+ (defence/level);
}
void Monster::info()//метод обычного класса
{
    cout << name << " " << level;
    cout << "lvl" << endl;
    cout << health << "remains" << endl;
    cout << "Attack=" << attack << " Health=" << health << endl;
}

void Monster :: heal()//метод обычного класса
{
    health += level * 3;
}

void Monster :: lvlup()//метод обычного класса
{
    level++;
    health = health + (health/level);
    attack = attack + (attack/level);
    defence = defence+ (defence/level);
    
    info();
}
void Monster :: seth(int a)//метод изменения protected поля
{
    health = health - a;
}

int Monster :: getd()//аксессор
{
    return defence;
}

int Monster :: geta()
{
    return attack;
}

int Monster :: geth()
{
    return health;
}

Tank :: Tank()//описание конструктора по умолчанию класса наследника
{
    level = 1;
    defence = 7;
    attack = 3;
    health = 70;
    name = "Tank";
}

void Tank :: MonsterA (Monster a)//описание переопределения виртуальной функции
{
    a.seth(attack - a.getd());
    a.info();
}

void Tank :: MonsterD (Monster a)
{
    health = health - (a.geta() - defence);
    info();
}

Gun :: Gun()//описание конструктора по умолчанию класса наследника
{
    level = 1;
    defence = 2;
    attack = 8;
    health = 30;
    name = "Gun";
}

void Gun :: MonsterA (Monster a)
{
    a.seth(attack - a.getd());
    a.info();
}

void Gun :: MonsterD (Monster a)
{
    health = health - (a.geta() - defence);
    info();
}

Striker :: Striker()
{
    level = 1;
    defence = 5;
    attack = 5;
    health = 50;
    name = "Striker";
}

void Striker :: MonsterA (Monster a)
{
    a.seth(attack - a.getd());
    a.info();
}

void Striker :: MonsterD (Monster a)
{
    health = health - (a.geta() - defence);
    info();
}

void Tank :: FightK(Monster b)//описание метода класса наследника
{
    while ((health!=0)&&(b.geth()!=0))
    {
        MonsterA(b);
        MonsterD(b);
    }
    if (health > 0)
    {
        lvlup();
        info();
    }
    else
        cout << "WASTED" << endl;
}

void Gun :: FightM(Monster b)
{
    while ((health!=0)&&(b.geth()!=0))
    {
        MonsterA(b);
        MonsterD(b);
    }
    if (health > 0)
    {
        lvlup();
        info();
    }
    else
        cout << "WASTED" << endl;
}

void Striker :: FightA(Monster b)
{
    while ((health!=0)&&(b.geth()!=0))
    {
        MonsterA(b);
        MonsterD(b);
    }
    if (health > 0)
    {
        lvlup();
        info();
    }
    else
        cout << "WASTED" << endl;
}
