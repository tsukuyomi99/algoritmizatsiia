#include <iostream>

using namespace std;

class Coordinat//класс
{
private:
    int x;
    int y;
public:
    
    Coordinat()//конструктор по умолчанию
    {};
    
    Coordinat(int k, int l) :x(k), y(l)//конструктор с парметрами
    {};
    
    Coordinat(Coordinat &a) :x(a.x), y(a.y)//конструктор копирования
    {};
    
    int getX() const//аксессор
    {
        return x;
    }
    
    int getY() const//аксессор
    {
        return y;
    }
    
    friend Coordinat operator+(Coordinat a, Coordinat b) ;//дружественная функция, внешняя функция
    friend Coordinat operator-(Coordinat a, Coordinat b);//дружественная функция, внешняя функция
    friend int operator*(Coordinat a, Coordinat b);//дружественная функция, внешняя функция
    
};
